#!/bin/bash

echo 'Initialising Helm'
helm init --wait --upgrade

echo 'adding qlik-stable helm repo'
helm repo add qlik-stable https://qlik.bintray.com/stable

#echo 'adding qlik-internal helm repo'
#helm repo add qlik-internal https://qlik.bintray.com/internal

#echo 'adding qlik-edge helm repo'
#helm repo add qlik-edge https://qlik.bintray.com/edge

echo 'Update helm repos'
helm repo update

#----------
echo "Create QMI StorageClass from NFS location"
hostname=`hostname`
storageClassName="qmistorage"
helm upgrade --install $storageClassName stable/nfs-client-provisioner --set nfs.server=$hostname --set nfs.path=/export/k8s --set storageClass.name=$storageClassName
#----------

#echo "Create Persistent Volume Claims for storage"
#kubectl apply -f /vagrant/files/pvc-mongo.yaml
#kubectl apply -f /vagrant/files/pvc-redis.yaml

sleep 10

#echo "Install MongoDB"
#helm install -n mycustommongo stable/mongodb -f /vagrant/files/mongo.yaml
#sleep 20
#-----------

echo 'Qlik Sense INIT - Custom Resource Definition'
helm install -n qliksense-init qlik-stable/qliksense-init

echo 'Installing QlikSense from Stable repo'
helm install -n qliksense qlik-stable/qliksense -f /vagrant/files/values.yaml 
